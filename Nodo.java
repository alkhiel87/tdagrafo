package tda.grafo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Nodo {
    
  String nombre;
  Arco adyacente; 
  ArrayList <Arco> adyacentenodo = new ArrayList<Arco>();


  public String getNombre (){
     return this.nombre; 
  }
  
  public void setNombre (String Nom){
     this.nombre=Nom;
  }
  
  public Arco getAdyacentes (){
     return this.adyacente; 
  }
  
  public void setAdyacentes (Arco Ad){ 
     this.adyacente=Ad;
  }
  
  private void ListarAdyacentes(Nodo A){
      Iterator<Arco> Puntero= A.adyacentenodo.iterator();
      while (Puntero.hasNext()){
      Arco Adyacente = Puntero.next();
      System.out.println("Origen: "+Adyacente.Origen+"  Destino: "+Adyacente.Destino+"  Peso: "+Adyacente.Peso);
      }
  }
  
  private boolean EsAdyacente(Nodo A, Nodo B){
   Iterator<Arco> Puntero= B.adyacentenodo.iterator();
   String Nombre= A.nombre;
   String NombreO;
   String NombreD;
    while (Puntero.hasNext()){
      Arco Adyacente = Puntero.next();
      NombreO=String.valueOf(B.adyacente.Origen);
      NombreD=String.valueOf(B.adyacente.Destino);
      if(Nombre.equals(NombreD)== true || Nombre.equals(NombreO)==true){
         return true; 
      }
    }
   return false;   
  } 
  
  private void RenombrarNodo (Nodo A, String Nom){
  String NombreAntiguo=A.nombre;
  Iterator<Arco> Puntero= A.adyacentenodo.iterator(); 
  A.nombre=Nom;
  while(Puntero.hasNext()){
  int x=0;    
  Arco Adyacente = Puntero.next();
      if(NombreAntiguo.equals(Adyacente.Destino.nombre)==true){
         Adyacente.Destino.nombre=Nom;
         A.adyacentenodo.set(x, Adyacente);
      }else{
        if(NombreAntiguo.equals(Adyacente.Origen.nombre)==true){
         Adyacente.Origen.nombre=Nom;
         A.adyacentenodo.set(x, Adyacente);
        }   
      }      
  x++;
  }
  }
  
  public void ModificarAdyacente(Nodo A, Nodo Destino, Nodo Origen, int Peso){
  Scanner Res = new Scanner(System.in);
  Iterator<Arco> Puntero= A.adyacentenodo.iterator();
  String S="Si";
  String N="No";
  while(Puntero.hasNext()){
   int x=0;    
   Arco Adyacente = Puntero.next();
   System.out.println("¿Desea Modificar este Adyacente?, Ingrese Si o No");
   String Respuesta = Res.next();
   if (Respuesta.equals(S)==false || Respuesta.equals(N)==false){
       System.out.println("Ingrese Si o No como respuesta porfavor");
   }
   if (Respuesta.equals(S)==true){
       Adyacente.Destino = Destino;
       Adyacente.Origen = Origen;
       Adyacente.Peso = Peso;
       A.adyacentenodo.set(x, Adyacente);
   }      
  x++;
  }
  }
  
  public void AgregarAdyacente (Nodo A, Nodo Origen, Nodo Destino, int Peso ){
        
      Arco Adyacente= new Arco();
      Adyacente.Origen= Origen;
      Adyacente.Destino= Destino;
      Adyacente.Peso= Peso;
      A.adyacentenodo.add(Adyacente);    
       
  } 
  
  public void EliminarAdyacente (Nodo A){
  Iterator<Arco> Puntero= A.adyacentenodo.iterator();
  int x;
  String Respuesta;
  String S;
  String N;
  N="No";
  S="Si";
  Scanner Res = new Scanner(System.in);
  while (Puntero.hasNext()){
      Arco Adyacente = Puntero.next();
      System.out.println(Adyacente.Origen);
      System.out.println(Adyacente.Destino);
      System.out.println(Adyacente.Peso);
      System.out.println("¿Desea Eliminar este Adyacente?, Ingrese Si o No");
      Respuesta = Res.next();
      if (Respuesta.equals(S)==false || Respuesta.equals(N)==false){
          System.out.println("Ingrese Si o No como respuesta porfavor");
      }
      if (Respuesta.equals(S)==true){
       Puntero.remove();
      }
  }
  }
}
   
 
   
 
