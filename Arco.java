package tda.grafo;
public class Arco {

Nodo Origen;
Nodo Destino;
int Peso;

public Nodo getOrigen (){
    return this.Origen; 
  }
  public void setOrigen (Nodo Org){
     this.Origen=Org;
  }
  
   public Nodo getDestino (){
    return this.Destino; 
  }
   
  public void setDestino (Nodo Des){
     this.Destino=Des;
  }
  
   public int getPeso (){
    return this.Peso; 
  }
   
  public void setPeso (int Pes){
     this.Peso=Pes;
  }
  
  private void ActualizarPeso(Arco B, int P){
   B.Peso=P; 
  }
}